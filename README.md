# README #

Qui c'è uno "scheletro" di documento LaTeX che potete usare per preparare le soluzioni ai problemi di ammissione degli stage delle Olimpiadi della Matematica.

Trovate un esempio di documento, contenente istruzioni e link utili, in `template.tex`.

Potete compilarlo con un qualunque compilatore LaTeX (vedete istruzioni all'interno per procuravene uno). Il file `stagetex.cls` deve stare nella stessa cartella dove metterete il vostro documento.

Buon lavoro^3!

#### Disclaimer

I file `mathtools.sty` e `mhsetup.sty` sono parte del pacchetto Mathtools, (c) 2004-2011 Morten Høgholm, 2012- Lars Mad­sen, disponibile su http://www.ctan.org/pkg/mathtools.

Il file `lineno.sty` è parte del pacchetto lineno, (c) 2002-2005 Uwe Lückm disponibile su http://www.ctan.org/pkg/lineno.
