\NeedsTeXFormat{LaTeX2e}[1995/06/01]
\ProvidesClass{stagetex}[2014/08/30 v1.1 - LaTeX class for Italian Math Olympiad training camps]
\typeout{Copyright (c) 2014--2015, F. Poloni}
\LoadClass[12pt,titlepage]{paper}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage{microtype}
\RequirePackage{color}
\RequirePackage[a4paper,top=4cm,bottom=4cm,left=3cm,right=3cm]{geometry}
\RequirePackage{lastpage}
\RequirePackage[italian]{babel}
\RequirePackage{amsmath,amsthm,amssymb,mathtools}
\RequirePackage{enumitem}
\RequirePackage{placeins}

%numeri di riga
%\RequirePackage[right]{lineno}

\RequirePackage[colorlinks=true,linkcolor=black,pdfpagelabels]{hyperref}

% variabili impostabili dall'utente
\def\nomest@ge{\textcolor{red}{Nome stage non definito!}}
\def\nomest@gista{\textcolor{red}{Nome stagista non definito!}}
\def\nomescuol@{\textcolor{red}{Nome scuola non definito!}}
\def\tipo@esercizi{\textcolor{red}{Tipo esercizi non definito!}}
\def\anno@corso{\textcolor{red}{Anno di corso non definito!}}
\def\genere@{\textcolor{red}{Genere non definito!}}

%istruzioni per impostarle
\newcommand{\Stage}[1]{\def\nomest@ge{#1}}
\newcommand{\Stagista}[1]{\def\nomest@gista{#1}}
\newcommand{\Esercizi}[1]{\def\tipo@esercizi{#1}}
\newcommand{\Scuola}[1]{\def\nomescuol@{#1}}
\newcommand{\AnnoCorso}[1]{\def\anno@corso{#1}}
\newcommand{\Genere}[1]{\def\genere@{#1}}

% header e footer, nello stesso stile di quelli di Max
\renewcommand{\@oddfoot}{\hfill\fbox{\textsf{Ammissione \nomest@ge\ --- Soluzioni esercizi
\tipo@esercizi\ --- Pag. \arabic{page} di \pageref{LastPage}}}\hfill}
\renewcommand{\@oddhead}{\textsf{\textbf{{\large \nomest@gista}}\hfill}}

%stile della pagina iniziale
\renewcommand{\maketitle}{
\begin{titlepage}
\pagenumbering{alph}
\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\begin{center} \Huge \textsf{ Esercizi di ammissione\\Olimpiadi della Matematica}
\end{center}
\begin{center}
    {\large 
    \renewcommand{\arraystretch}{1.4}
    \begin{tabular}{|r|l|}
        \hline
        \textsf{Stage:}  & \nomest@ge  \\
        \hline
        \textsf{Soluzioni di:} & \textbf{\nomest@gista}  \\
        \hline
        \textsf{Scuola:} & \parbox{20em}{\strut\nomescuol@\strut} \\
        \hline
        \textsf{Anno di corso\footnotemark[1]:} & \anno@corso\textsuperscript{o} \\
        \hline
        \textsf{Genere:} & \genere@ \\
        \hline        
        \textsf{Tipo di esercizi:}  & \tipo@esercizi  \\
        \hline
        \textsf{Data:} & \today  \\
        \hline
    \end{tabular}
    }
\end{center}
\tableofcontents
\vfill
\footnotetext[1]{Per gli stage Senior, si intende l'anno che state per \emph{iniziare}, non quello che avete gi\`a finito.
Il primo anno delle superiori \`e 1, il quinto (quello della maturit\`a per la maggior parte degli studenti) \`e 5. Attenetevi a questa numerazione
anche se frequentate un liceo classico.}

\end{titlepage}
\pagenumbering{arabic}
}

\newcounter{soluzione} %ci serve per poter usare @addtoreset con le equazioni.
\@addtoreset{equation}{soluzione}
\@addtoreset{lemma}{soluzione}

\newenvironment{soluzione}[1]{
\clearpage
%inizio dell'environment soluzione
\FloatBarrier
\begin{center}
    {\Large \textsf{\textbf{Soluzione problema \framebox{#1}}}}
    \stepcounter{soluzione}
    \phantomsection
\end{center}
\addcontentsline{toc}{section}{Soluzione problema #1}
\renewcommand{\theequation}{#1.\arabic{equation}}
\renewcommand{\thelemma}{#1.\arabic{lemma}}
\renewcommand{\theHequation}{#1.\arabic{equation}}
\renewcommand{\theHlemma}{#1.\arabic{lemma}}
%\begin{linenumbers}[1] %questa riga e l'end corrispondente vanno scommentate per avere linee numerate (oltre a includere lineno.sty)
}{
%fine dell'environment soluzione
%\end{linenumbers}
\FloatBarrier
}

%inserisce automaticamente la prima pagina all'inizio del documento
\AtBeginDocument{\maketitle}

%ambiente lemma
\newtheorem{lemma}{Lemma}

% abs e norm, per comodità
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}

%lista per soluzioni a step
\newcounter{step@}
\newenvironment{steplist}{\begin{steplist@}}{\end{steplist@} \setcounter{step@}{0}}
\newlist{steplist@}{description}{1}
\setlist[steplist@]{align=left, leftmargin=0pt, labelindent=0pt,listparindent=\parindent, labelwidth=0pt, itemindent=!}
\newcommand{\step}[1]{\item[\stepcounter{step@}\textsf{\textbf{Step \arabic{step@}:}} \textbf{#1}]}

% titolo della ToC
\addto\captionsitalian{
\renewcommand{\contentsname}{Indice delle soluzioni}
}

